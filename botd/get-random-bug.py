#!/usr/bin/python3

from sys import stderr, exit
import psycopg2
import re
from datetime import datetime
import email.utils
import sqlite3 as lite

NOTONSALSARESTRICTION="AND (vcs_url NOT ilike '%salsa%' OR vcs_browser not ILIKE '%salsa%' \
                            OR vcs_url IS NULL OR vcs_browser IS NULL \
                           )"
NOVCSRESTRICTION="AND (vcs_url IS NULL OR vcs_browser IS NULL \
                       OR vcs_url ilike '%anonscm.debian.org%' OR vcs_browser ilike '%anonscm.debian.org%' \
                       OR vcs_url ilike '%git.debian.org%' OR vcs_browser ilike '%git.debian.org%' \
                       OR vcs_url ilike '%svn%' OR vcs_browser ilike '%svn%' \
                       OR vcs_url ilike '%cvs%' OR vcs_browser ilike '%cvs%' \
                      )"
STANDARDSVERRESTRICTION="AND standards_version NOT like '4%'"

RESTRICTION=NOTONSALSARESTRICTION + " " + STANDARDSVERRESTRICTION
UPLOADDATERESTRICTION="AND u.last_upload < CURRENT_DATE - INTERVAL '5 years'"
#RESTRICTION=""

LIMIT=5

OUTFILE="botd.html"

# PORT = 5441
UDDPORT = 5452
PORT = UDDPORT
DEFAULTPORT = 5432

###############################################################################
connection_pars = [
#    {'service': 'udd'},
    {'host': 'localhost', 'port': PORT, 'user': 'guest', 'database': 'udd'},         ##FIXME: use this for development, than deactivate
#    {'host': 'localhost', 'port': DEFAULTPORT, 'user': 'guest', 'database': 'udd'},  ##FIXME: use this for development, than deactivate

    # Public UDD mirror for real queries
    {'host': 'udd-mirror.debian.net', 'port': 5432,
     'user': 'udd-mirror', 'password': 'udd-mirror', 'database': 'udd'},
]
conn = None
for par in connection_pars:
    try:
        conn = psycopg2.connect(**par)
        break
    except psycopg2.OperationalError as err:
        continue

if conn is None:
    stderr.write("Connection to UDD can't be established by any of\n  %s" % str(connection_pars))
    exit(-1)

conn.set_client_encoding('utf-8')
curs = conn.cursor()
# uddlog = open('logs/uddquery.log', 'w')

def RowDictionaries(cursor):
    """Return a list of dictionaries which specify the values by their column names"""

    if not cursor.description:
        # even if there are no data sets to return the description
        # should contain the table structure.  If not something went
        # wrong and we return None as to represent a problem
        return None

    try:
        return [dict((dd[0], dv) for (dd, dv) in zip(cursor.description, row))
                for row in cursor]
    except NameError:
        return [dict((dd[0], dv) for (dd, dv) in zip(cursor.description, row))
                for row in cursor]

def _execute_udd_query(query, arg=None):
    try:
        if arg:
            curs.execute(query, arg)
        else:
            curs.execute(query)
    except psycopg2.ProgrammingError as err:
        stderr.write("Problem with query\n%s\n%s\n" % (query, str(err)))
        exit(-1)
    except psycopg2.DataError as err:
        stderr.write("%s; query was\n%s\n" % (str(err), query))


query = """  SELECT b.id, b.source, s.maintainer, DATE(last_modified) AS lastmod, severity, StdVer, u.last_upload, vcs_url, title,
                    testsuite, homepage, (SELECT MAX(popcon.vote) FROM packages INNER JOIN popcon ON popcon.package = packages.package WHERE packages.source = b.source AND packages.release = 'sid') AS popcon,
                    regexp_replace(regexp_replace(s.version, E'\\\\+b[0-9]+$', ''), E'-[^-]+', '') AS version,
                    d.ustatus, d.upstream_version,
                    (SELECT STRING_AGG(bt1.tag, ', ') FROM bugs_tags bt1 WHERE bt1.id = b.id) AS tags,
    (SELECT DISTINCT description FROM packages WHERE source = b.source LIMIT 1) AS description
    FROM bugs b
    INNER JOIN (SELECT DISTINCT source, maintainer, standards_version AS StdVer, vcs_url, testsuite, homepage, version FROM sources
      WHERE release = 'sid' AND component = 'main'
        %s
      ) s ON s.source=b.source
    INNER JOIN (SELECT source, DATE(date) AS last_upload FROM (
                 SELECT source, date, ROW_NUMBER() OVER (PARTITION BY source ORDER BY date DESC) AS rank FROM upload_history WHERE nmu = 'f'
                 ) w WHERE rank = 1
               ) u ON u.source = b.source
    LEFT OUTER JOIN (SELECT source, upstream_version, status AS ustatus FROM upstream WHERE release = 'sid') d ON b.source = d.source
    WHERE status NOT IN ('done')
      AND NOT EXISTS (SELECT 1 FROM bugs_tags bt2 WHERE bt2.id = b.id AND bt2.tag = 'wontfix')
      %s
    ORDER BY RANDOM()
    LIMIT %i;
""" % ( RESTRICTION, UPLOADDATERESTRICTION, LIMIT )

_execute_udd_query(query)


html_header="""<!DOCTYPE html>
<html>
<head>
<title>Bug Of The Day</title>
<meta charset="utf-8" />
<meta name="author" content="Andreas Tille" />
<meta name="keywords" lang="de" content="Debian, Quality Assurance" />
<meta name="description" content="Squash a bug every day" />
<style type="text/css">
body{
    font-family: noto sans;
}
</style>
</head>
<body>
"""

html_footer="""
<p>
For more information about Bug of the Day see the according <a href="https://salsa.debian.org/tille/tiny_qa_tools/-/wikis/Tiny-QA-tasks">Wiki page</a>
and feel free to join our <a href="https://matrix.to/#/#debian-tiny-tasks:matrix.org">Matrix channel</a>.
</p>
</body>
</html>
"""

try:
    outcon = lite.connect('tinytasks.db')
    outcur = outcon.cursor()
except lite.Error as e:
    print("Error %s:" % e.args[0])
    exit(1)

today=datetime.today()
with open(OUTFILE, 'w') as file:
    file.write(html_header)
    file.write("<h1>Candidates for bug of the day from %i-%02i-%02i</h1>\n" % (today.year, today.month, today.day))
    for row in RowDictionaries(curs):
        row['date'] = today
        prepsql = "INSERT INTO bugs ({}) VALUES ({})".format(', '.join(row.keys()),
                                                         ', '.join([':' + key for key in row.keys()]))
        outcur.execute(prepsql, row)
        (mname, memail) = email.utils.parseaddr(row['maintainer'])
        row['maintainer'] = mname + ' &lt;' + memail + '&gt;'
        row['memail'] = memail
        row['versionmsg'] = ''
        if not row['tags']:
            row['tagstring'] = ""
        else:
            row['tagstring'] = "</tr><tr>\n<td></td><td>%s</td>" % row['tags']
        if not row['homepage']:
            row['homepagestring'] = "<em>No Homepage field set</em>"
        else:
            row['homepagestring'] = '<a href="%s">Homepage</a>' % row['homepage']
        if row['ustatus'] == 'newer package available':
            row['versionmsg'] = ', Debian version = %s while upstream has %s' % (row['version'], row['upstream_version'])
        else:
            if row['ustatus'] == 'error':
                row['versionmsg'] = ', uscan error'
        if row['popcon'] is None:
            row['popcon'] = "unknown"
        file.write("""
<p>
<table>
<tr>
<td><a href="https://bugs.debian.org/%(id)i">%(id)i</a>:</td><td><a href="https://tracker.debian.org/%(source)s">%(source)s</a>,
    <a href="https://qa.debian.org/developer.php?email=%(memail)s">%(maintainer)s</a></td>
</tr><tr>
<td>(%(severity)s)</td><td>%(title)s</td>
</tr><tr>
<td>%(lastmod)s</td><td>%(description)s</td>
</tr><tr>
<td></td><td>VCS: %(vcs_url)s, Popcon: %(popcon)s, StdVer: %(stdver)s, testsuite: %(testsuite)s, last upload of maintainer: %(last_upload)s, %(homepagestring)s%(versionmsg)s</td>
%(tagstring)s
</tr>
</table>
</p>
""" % row)

    file.write(html_footer)
    outcon.commit()
